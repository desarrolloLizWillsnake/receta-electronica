(function(jQuery){

    // start: Calendar =========
     $('.dashboard .calendar').fullCalendar({
        header: {
            left: 'prev,next today',
            center: 'title',
            right: 'month,agendaWeek,agendaDay'
        },
        defaultDate: '2016-02-12',
        lang: "es",
        businessHours: true, // display business hours
        editable: true,
        events: [
            {
                title: 'Comida de Negocios',
                start: '2016-02-03T13:00:00',
                constraint: 'businessHours'
            },
            {
                title: 'Reunion',
                start: '2016-02-13T11:00:00',
                constraint: 'availableForMeeting', // defined below
                color: '#20C572'
            },
            {
                title: 'Conferencia',
                start: '2016-02-18',
                end: '2016-02-20'
            },
            {
                title: 'Fiesta',
                start: '2016-02-29T20:00:00'
            },

            // areas where "Meeting" must be dropped
            {
                id: 'availableForMeeting',
                start: '2016-02-11T10:00:00',
                end: '2016-02-11T16:00:00',
                rendering: 'background'
            },
            {
                id: 'availableForMeeting',
                start: '2016-02-13T10:00:00',
                end: '2016-02-13T16:00:00',
                rendering: 'background'
            },

            // red areas where no events can be dropped
            {
                start: '2016-02-24',
                end: '2016-02-28',
                overlap: false,
                rendering: 'background',
                color: '#FF6656'
            },
            {
                start: '2016-02-06',
                end: '2016-02-08',
                overlap: true,
                rendering: 'background',
                color: '#FF6656'
            }
        ]
    });
    // end : Calendar==========

  })(jQuery);