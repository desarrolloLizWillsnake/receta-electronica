<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Main extends CI_Controller {

	function __construct()
	{
		parent::__construct();

		$this->output->set_template('main');

	}

	public function index() {

		$this->load->css('assets/themes/portal/css/plugins/font-awesome.min.css');
		$this->load->css('assets/themes/portal/css/plugins/simple-line-icons.css');
		$this->load->css('assets/themes/portal/css/plugins/animate.min.css');
		$this->load->css('assets/themes/portal/css/plugins/fullcalendar.min.css');
		$this->load->css('assets/themes/portal/css/style.css');

		$this->load->js('assets/themes/portal/js/plugins/moment.min.js');
		$this->load->js('assets/themes/portal/js/plugins/fullcalendar.min.js');
		$this->load->js('assets/themes/portal/js/plugins/lang-all.js');
		$this->load->js('assets/themes/portal/js/plugins/jquery.nicescroll.js');
		$this->load->js('assets/themes/portal/js/plugins/jquery.vmap.min.js');
		$this->load->js('assets/themes/portal/js/plugins/maps/jquery.vmap.world.js');
		// $this->load->js('assets/themes/portal/js/plugins/maps/jquery.vmap.sampledata.js');
		// $this->load->js('assets/themes/portal/js/plugins/maps/chart.min.js');
		$this->load->js('assets/themes/portal/js/main.js');
		$this->load->js('assets/themes/portal/js/indicePortal.js');

		$this->load->view('front/main');
	}

}
