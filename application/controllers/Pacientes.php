<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Pacientes extends CI_Controller {

	function __construct() {
		parent::__construct();

		$this->output->set_template('main');
	}

	public function index() {

		$this->load->css('assets/themes/portal/css/plugins/font-awesome.min.css');
		$this->load->css('assets/themes/portal/css/plugins/datatables.bootstrap.min.css');
		$this->load->css('assets/themes/portal/css/plugins/animate.min.css');		
		$this->load->css('assets/themes/portal/css/style.css');


		$this->load->js('assets/themes/portal/js/plugins/moment.min.js');
		$this->load->js('assets/themes/portal/js/plugins/jquery.datatables.min.js');
		$this->load->js('assets/themes/portal/js/plugins/datatables.bootstrap.min.js');
		$this->load->js('assets/themes/portal/js/plugins/jquery.nicescroll.js');
		$this->load->js('assets/themes/portal/js/main.js');
		$this->load->js('assets/themes/portal/js/pacientes/indicePacientes.js');

		$this->load->view('front/pacientes/main');
	}

	public function tabla_pacientes() {
		$this->output->unset_template();
    
        $this->datatables->select("id_datos_paciente,
        							no_seguro,
        							CONCAT(nombre,' ',apellido_pa, ' ', apellido_ma) AS nombre,
        							email,
        							telefono")
                ->from('datos_paciente')
                ->add_column('acciones', '<a href="'.base_url("pacientes/generar_receta/$1").'"><i class="fa fa-print"></i></a>', 'id_datos_paciente');


        echo $this->datatables->generate();
	}

	public function agregar_paciente() {
		$this->load->css('assets/themes/portal/css/plugins/font-awesome.min.css');
		$this->load->css('assets/themes/portal/css/plugins/animate.min.css');		
		$this->load->css('assets/themes/portal/css/style.css');


		$this->load->js('assets/themes/portal/js/plugins/moment.min.js');
		$this->load->js('assets/themes/portal/js/plugins/jquery.nicescroll.js');
		$this->load->js('assets/themes/portal/js/main.js');
		$this->load->js('assets/themes/portal/js/pacientes/indicePacientes.js');

		$this->load->view('front/pacientes/agregar_paciente');
	}

	public function generar_receta() {
		$this->load->css('assets/themes/portal/css/plugins/font-awesome.min.css');
		$this->load->css('assets/themes/portal/css/plugins/animate.min.css');		
		$this->load->css('assets/themes/portal/css/style.css');


		$this->load->js('assets/themes/portal/js/plugins/moment.min.js');
		$this->load->js('assets/themes/portal/js/plugins/jquery.nicescroll.js');
		$this->load->js('assets/themes/portal/js/main.js');
		$this->load->js('assets/themes/portal/js/pacientes/indicePacientes.js');

		$this->load->view('front/pacientes/generar_receta');
	}

}