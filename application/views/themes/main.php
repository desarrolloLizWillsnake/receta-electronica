<!DOCTYPE html>
<html lang="es">
	<head>
		<title><?php echo $title; ?></title>
		<meta charset="utf-8">
		<meta name="description" content="Miminium Admin Template v.1">
		<meta name="author" content="Isna Nur Azis">
		<meta name="keyword" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- CSS -->
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/bootstrap.min.css" rel="stylesheet">

		<?php

			if(!empty($meta))
			foreach($meta as $name=>$content){
				echo "\n\t\t";
				?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
					 }
			echo "\n";

			if(!empty($canonical))
			{
				echo "\n\t\t";
				?><link rel="canonical" href="<?php echo $canonical?>" /><?php

			}
			echo "\n\t";

			foreach($css as $file){
			 	echo "\n\t\t";
				?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
			} echo "\n\t";
		?>

		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/portal/img/logomi.png">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

  <body id="mimin" class="dashboard">
  	<!-- start: Header -->
    <nav class="navbar navbar-default header navbar-fixed-top">
      <div class="col-md-12 nav-wrapper">
        <div class="navbar-header" style="width:100%;">
          <div class="opener-left-menu is-open">
            <span class="top"></span>
            <span class="middle"></span>
            <span class="bottom"></span>
          </div>
            <a href="" class="navbar-brand"> 
             <b></b>
            </a>

          <ul class="nav navbar-nav navbar-right user-nav">
            <li class="user-name"><span>Dr. Psiquiatra</span></li>
              <li class="dropdown avatar-dropdown">
               <img src="<?php echo base_url(); ?>assets/themes/portal/img/avatar.jpg" class="img-circle avatar" alt="user name" data-toggle="dropdown" aria-haspopup="true" aria-expanded="true"/>
               <ul class="dropdown-menu user-dropdown">
                 <li><a href=""><span class="fa fa-user"></span> Perfil</a></li>
                 <li><a href=""><span class="fa fa-calendar"></span> Calendario</a></li>
                 <li role="separator" class="divider"></li>
                 <li class="more">
                  <ul>
                    <li><a href=""><span class="fa fa-cogs"></span></a></li>
                    <li><a href=""><span class="fa fa-lock"></span></a></li>
                    <li><a href=""><span class="fa fa-power-off "></span></a></li>
                  </ul>
                </li>
              </ul>
            </li>
            <!-- <li ><a href="#" class="opener-right-menu"><span class="fa fa-coffee"></span></a></li> -->
          </ul>
        </div>
      </div>
    </nav>
    <!-- end: Header -->

    <!-- start:Left Menu -->
    <div id="left-menu">
      <div class="sub-left-menu scroll">
        <ul class="nav nav-list">
            <li><div class="left-bg"></div></li>
            <li class="time">
              <h1 class="animated fadeInLeft">21:00</h1>
              <p class="animated fadeInRight">Sat,October 1st 2029</p>
            </li>

            <li class="ripple"><a href=""><span class="fa fa-calendar-o"></span>Citas</a></li>

            <li class="ripple"><a href="<?php echo base_url(); ?>pacientes"><span class="fa fa-male"></span>Pacientes</a></li>

            <li class="ripple"><a href=""><span class="fa fa-area-chart"></span>Reportes</a></li>
            

          </ul>
        </div>
    </div>
    <!-- end: Left Menu -->


  	<?php echo $output;?>
   
	<script src="<?php echo base_url(); ?>assets/themes/portal/js/jquery.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themes/portal/js/jquery.ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themes/portal/js/bootstrap.min.js"></script>

  <?php
  foreach($js as $file){
      echo "\n\t\t";
  ?><script src="<?php echo $file; ?>"></script><?php
  } echo "\n\t";
  ?>

  </body>
</html>
