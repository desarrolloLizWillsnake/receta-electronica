<!DOCTYPE html>
<html lang="es">
	<head>
		<title><?php echo $title; ?></title>
		<meta charset="utf-8">
		<meta name="description" content="Miminium Admin Template v.1">
		<meta name="author" content="Isna Nur Azis">
		<meta name="keyword" content="">
		<meta name="viewport" content="width=device-width, initial-scale=1">
		
		<!-- CSS -->
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/bootstrap.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/plugins/font-awesome.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/plugins/simple-line-icons.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/plugins/animate.min.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/plugins/icheck/skins/flat/aero.css" rel="stylesheet">
		<link href="<?php echo base_url(); ?>assets/themes/portal/css/style.css" rel="stylesheet">

		<?php

			if(!empty($meta))
			foreach($meta as $name=>$content){
				echo "\n\t\t";
				?><meta name="<?php echo $name; ?>" content="<?php echo $content; ?>" /><?php
					 }
			echo "\n";

			if(!empty($canonical))
			{
				echo "\n\t\t";
				?><link rel="canonical" href="<?php echo $canonical?>" /><?php

			}
			echo "\n\t";

			foreach($css as $file){
			 	echo "\n\t\t";
				?><link rel="stylesheet" href="<?php echo $file; ?>" type="text/css" /><?php
			} echo "\n\t";

			foreach($js as $file){
					echo "\n\t\t";
					?><script src="<?php echo $file; ?>"></script><?php
			} echo "\n\t";
		?>

		<link rel="shortcut icon" href="<?php echo base_url(); ?>assets/themes/portal/img/logomi.png">
		<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
		<!--[if lt IE 9]>
			<script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
			<script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
		<![endif]-->
	</head>

  <body id="mimin" class="dashboard form-signin-wrapper">
  	<?php echo $output;?>

  	<script src="<?php echo base_url(); ?>assets/themes/portal/js/jquery.min.js"></script>
  	<script src="<?php echo base_url(); ?>assets/themes/portal/js/jquery.ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themes/portal/js/bootstrap.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/themes/portal/js/plugins/moment.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/themes/portal/js/plugins/icheck.min.js"></script>

	<script src="<?php echo base_url(); ?>assets/themes/portal/js/main.js"></script>

  </body>
</html>
