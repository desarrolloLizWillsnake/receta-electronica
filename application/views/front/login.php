<div class="container">
  <form class="form-signin">
    <div class="panel periodic-login">
      <!-- <span class="atomic-number">28</span> -->
      <div class="panel-body text-center">
        <!-- <h1 class="atomic-symbol">Mi</h1>
        <p class="atomic-mass">14.072110</p>
        <p class="element-name">Miminium</p> -->

        <!-- <i class="icons icon-arrow-down"></i> -->
        <div class="form-group form-animate-text" style="margin-top:40px !important;">
          <input type="text" class="form-text" required>
          <span class="bar"></span>
          <label>Usuario</label>
        </div>
        <div class="form-group form-animate-text" style="margin-top:40px !important;">
          <input type="password" class="form-text" required>
          <span class="bar"></span>
            <label>Contraseña</label>
          </div>
          <label class="pull-left">
            <input type="checkbox" class="icheck pull-left" name="checkbox1"/> Recordarme
          </label>
          <!-- <input type="submit" class="btn col-md-12" value="Entrar"/> -->
          <a href="<?php echo base_url(); ?>main" class="btn col-md-12" />Entrar</a>
      </div>
      <div class="text-center" style="padding:5px;">
        <a href="">¿Olvidó su Contraseña'</a>
        <a href="">| Registrarme</a>
      </div>
    </div>
  </form>
</div>

<!-- Fin de contenido -->

<script type="text/javascript">
 $(document).ready(function(){
   $('input').iCheck({
    checkboxClass: 'icheckbox_flat-aero',
    radioClass: 'iradio_flat-aero'
  });
 });
</script>