<div class="container-fluid mimin-wrapper">
    <div id="content">
       <div class="panel box-shadow-none content-header">
          <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Agregar Paciente</h3>
            </div>
          </div>
      </div>
      <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading"><h3>Datos del Paciente</h3></div>
            <div class="panel-body">

              <div class="row">
                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Nombre</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Apellido Paterno</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Apellido Materno</label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>No. Seguro Social</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Peso</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Estatura</label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Correo Electrónico</label>
                  </div>
                </div>

                <div class="col-md-6">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Telefono</label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-md-offset-5">
                  <input type="button" class=" btn btn-3d btn-primary" value="Guardar Paciente"/>
                </div>
              </div>

          </div>
        </div>
      </div>


      <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading"><h3>Generar Receta</h3></div>
            <div class="panel-body">

              <div class="row">
                <div class="col-md-4">
                  <div class="form-group"><label class="col-sm-2 control-label text-right">Folio</label>
                    <div class="col-sm-10"><input type="text" class="form-control" disabled value="12345"></div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group"><label class="col-sm-2 control-label text-right">Lugar</label>
                    <div class="col-sm-10"><input type="text" class="form-control" disabled value="Jurisdicción Sanitaria Unidad Médica"></div>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group"><label class="col-sm-2 control-label text-right">Clave</label>
                    <div class="col-sm-10"><input type="text" class="form-control" disabled value="JS H1"></div>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Medicamento</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Cantidad</label>
                  </div>
                </div>

                <div class="col-md-4">
                  <div class="form-group form-animate-text">
                    <input type="text" class="form-text" required>
                    <span class="bar"></span>
                    <label>Modo de uso</label>
                  </div>
                </div>
              </div>

              <div class="row">
                <div class="col-md-12 col-md-offset-5">
                  <input type="button" class=" btn btn-3d btn-primary" value="Imprimir Receta"/>
                </div>
              </div>

          </div>
        </div>
      </div>  

    </div>
  </div>
</div>