<div class="container-fluid mimin-wrapper">
    <div id="content">
       <div class="panel box-shadow-none content-header">
          <div class="panel-body">
            <div class="col-md-12">
                <h3 class="animated fadeInLeft">Pacientes</h3>
            </div>
          </div>
      </div>
      <div class="col-md-12 top-20 padding-0">
        <div class="col-md-12">
          <div class="panel">
            <div class="panel-heading"><h3>Lista de Pacientes</h3></div>
            <div class="panel-body">

              <div class="row">
                <div class="col-md-3">
                  <button class="btn ripple btn-3d btn-success" id="agregar_paciente">
                    <div>
                      <span>Agregar Paciente</span>
                    </div>
                  </button>
                </div>
              </div>

              <br />
              <br />

              <div class="row">
                <div class="responsive-table">
                  <table id="tabla_pacientes" class="table table-striped table-bordered" width="100%" cellspacing="0">
                    <thead>
                      <tr>
                        <th>ID</th>
                        <th>No. Seguro Social</th>
                        <th>Nombre</th>
                        <th>Email</th>
                        <th>Tel+efono</th>
                        <th>Acciones</th>
                      </tr>
                    </thead>
                    <tbody>
                      
                    </tbody>
                  </table>
                </div>
              </div>
          </div>
        </div>
      </div>  
    </div>
  </div>
</div>